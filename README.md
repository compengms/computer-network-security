# Computer Network Security

|          |Available|Last update|
|----------|:-------:|:---------:|
|Exams     |yes      |2018       |
|Notes     |yes      |2020       |
|References|yes      |           |
|Slides    |yes      |2018       |

## Notes

|Author   |Last update|Format   |Reference|
|---------|:---------:|:-------:|:-------:|
|Matteo   |2020       |TeX / PDF|[Source code](https://github.com/MatteoSalvino/CNS-notes)|
|Giuliano |2019       |TeX / PDF|[Source code](https://github.com/GiulianoAbruzzo/MSECS-Sapienza-Notes)|
|Francesco|2018       |PDF      |n/a|